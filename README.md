# math-bot

[![pipeline status](https://gitlab.com/tirtow/math-bot/badges/master/pipeline.svg)](https://gitlab.com/tirtow/math-bot/commits/master)

A Discord bot for the Division 2

- Python version: 3.6.7
- Discord.py version: 1.0.1

## Setup
1. Install the requirements
```
$ pip3 install -r requirements.txt
```
2. Create a [Discord Bot](https://discordapp.com/developers/applications)
3. Set the `MATH_BOT_TOKEN` environment variable to the bot's token
```
$ export MATH_BOT_TOKEN=YOUR_BOT_TOKEN
```
4. Run the bot
```
$ python3 main.py
```
