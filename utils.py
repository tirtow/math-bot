from datetime import datetime
import importlib
from os import listdir
from os.path import dirname, join, realpath


def log(message, newline=False):
    """
    Logs the message to the console with the current time

    message     -- the message to log
    newline     -- whether or not to print a blank line (default False).
    """

    print("[" + str(datetime.now()) + "]: " + message)
    if newline:
        print("[" + str(datetime.now()) + "]:")


def load_commands(client):
    """
    Uses reflection to get an instance of all Commands.

    client  -- the client running the bot

    Returns a dict mapping the name of the command to the instance of the
    command.
    """

    result = {}
    path = join(dirname(realpath(__file__)), "commands")
    for f in listdir(path):
        if f.endswith("_command.py"):
            name = f[:-3]               # Get rid of the .py
            split = name.split("_")     # Split to before and after _

            # Import the module
            module = importlib.import_module("commands." + "_".join(split))

            # Yield the class type
            name = "".join(map(lambda s: s.title(), split))
            instance = getattr(module, name)(client)
            result[name] = instance

    return result


def load_exception_handlers(client):
    """
    Uses reflection to get an instance of all ExceptionHandlers.

    client  -- the client running the bot

    Returns a dict mapping the type of the exception handled to the
    ExceptionHandler.
    """

    result = {}
    path = join(dirname(realpath(__file__)), "exception_handlers")
    for f in listdir(path):
        if f.endswith("_exception_handler.py"):
            name = f[:-3]               # Get rid of the .py
            split = name.split("_")     # Split into words

            # Import the module
            module = importlib.import_module("exception_handlers." +
                                             "_".join(split))

            # Create instance of class
            name = "".join(map(lambda s: s.title(), split))
            instance = getattr(module, name)(client)
            result[instance.exception_type()] = instance

    return result


def secs_to_human(seconds):
    """
    Converts the given seconds into a human readable format

    seconds     -- the seconds to convert

    Returns the formatted string
    """

    secs = seconds % 60
    seconds //= 60
    minutes = seconds % 60
    seconds //= 60
    hours = seconds % 24
    days = seconds // 24

    return "%sd %sh %sm %ss" % (str(days), str(hours), str(minutes), str(secs))


def compare_values(lhs, rhs):
    """
    Gets the comparison string for the two given values

    lhs     -- the left value
    rhs     -- the right value

    Returns the comparison string
    """

    diff = lhs - rhs

    if diff < 0:
        return " (%s)" % str(diff)
    elif diff > 0:
        return " (+%s)" % str(diff)

    return " (-)"


def compare_times(lhs, rhs):
    """
    Gets the comparison string for the two given playtimes

    lhs     -- the left playtime
    rhs     -- the right playtime

    Returns the comparison string
    """

    # Find the difference in hours
    diff = lhs - rhs

    if diff < 0:
        return " (-%s)" % secs_to_human(abs(diff))
    elif diff > 0:
        return " (+%s)" % secs_to_human(diff)

    return " (-)"
