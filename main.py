import discord
from discord.ext.commands import Bot
import os
import platform

from commands.command import Command
from command_handler import CommandHandler
from utils import log


# The command handler for the bot
handler = None

# Create the discord bot client
client = Bot(description="Bot by tirtow#6252", command_prefix=Command.PREFIX)


@client.event
async def on_ready():
    """
    Performs the startup for the bot.
    """

    global handler

    # Set the playing message
    log("Setting playing message")
    game = discord.Game(name=Command.PREFIX + "help")
    await client.change_presence(activity=game)

    # Initialize the CommandHandler
    handler = CommandHandler(client)

    _print_startup_message()


@client.event
async def on_message(message):
    """
    Handles checking for commands.

    Checks each message to see if it matches one of the commands that the bot
    responds to handling the command if it finds one.

    message -- the discord.Message that was received
    """

    command = message.content.lower()
    if command == Command.PREFIX + "ping":
        # Ping command to check if bot is alive and responding
        await message.channel.send("Pong!")
    elif command.startswith(Command.PREFIX):
        # Handle the command
        await handler.handle(message)


def _print_startup_message():
    """
    Prints the startup message.
    """

    print("Current Discord.py Version: {} | Current Python Version: {}"
          .format(discord.__version__, platform.python_version()))
    print("--------")
    print("Use this link to invite {}:".format(client.user.name))
    print("https://discordapp.com/oauth2/authorize?client_id={}&scope=bot"
          "&permissions=8".format(client.user.id))
    print("--------")
    print("--------")
    print("Created by tirtow#6252\n")


# Run the bot
if __name__ == "__main__":
    client.run(os.environ["MATH_BOT_TOKEN"])
