from datetime import datetime
import discord
from enum import Enum


class Color(Enum):
    """
    The colors for embeds
    """

    SUCCESS = 0x269B42
    ERROR = 0xF44242
    INFO = 0x4286F4


class MathEmbed(discord.Embed):
    """
    Extends discord.Embed to add footer information to the Embed
    """

    def __init__(self, user, color, **kwargs):
        """
        Builds the Embed

        user    -- the discord.User to put in the footer
        kwargs  -- the args to use in the Embed
        """

        kwargs["color"] = color.value
        super().__init__(timestamp=datetime.now(), **kwargs)
        url = user.avatar_url
        if url == "":
            url = user.default_avatar_url
        self.set_footer(text=str(user), icon_url=url)
