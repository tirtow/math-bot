from commands.command import Command
from math_embed import Color, MathEmbed
from utils import log, load_commands, load_exception_handlers


class CommandHandler:
    """
    Handles responding to any command sent to the bot.

    Uses reflection to load in the Commands and ExceptionHandlers for the bot
    and checks those Commands to see which matches the message that the bot
    received.
    """

    def __init__(self, client):
        """
        Initializes this CommandHandler.

        Loads in the commands and exception handlers for the bot to run.

        client  --  the client running the bot
        """

        self.client = client
        self.commands = load_commands(client)
        self.exception_handlers = load_exception_handlers(client)

    async def handle(self, message):
        """
        Handles the given message.

        Checks if the message is a command that the bot responds to. If so,
        passes control to that Command to handle the message.

        message     -- the discord.Message received
        """

        command = message.content.lower().replace(Command.PREFIX, "", 1)
        if len(command) == 0:
            # Invalid command, print the help message
            log("Invalid command. Printing help message")
            await self.commands["HelpCommand"].handle(message)
        else:
            try:
                # Find the command and handle it
                split = command.split()
                op = split[0]
                for name, command in self.commands.items():
                    if command.is_alias(op):
                        log("%s issued command '%s'" % (str(message.author),
                                                        message.content))
                        await command.handle(message)
                        break
                else:
                    # No commands matched, doing nothing
                    log("Did not match any commands.")
            except Exception as e:
                # Handle any exceptions received
                log("Caught exception %s" % str(e))
                if type(e) in self.exception_handlers:
                    await self.exception_handlers[type(e)].handle(message,
                                                                  str(e))
                else:
                    log("Received unhandled error of type %s: %s" %
                        (str(type(e)), str(e)))

                    # Message channel
                    await self._send_unhandled_exception(message)

    async def _send_unhandled_exception(self, message):
        """
        Sends message saying something went wrong to the channel message was
        received from

        message     -- the discord.Message that was received
        """

        msg = "A robot monkey broke something. It will be dismantled and " \
              "sold for scrap and another robot monkey will fix the issue as " \
              "soon as possible."
        embed = MathEmbed(self.client.user,
                          color=Color.ERROR,
                          title="Something went wrong",
                          description=msg)
        await message.channel.send(embed=embed)
