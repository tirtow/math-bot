from commands.command import Command
from math_embed import Color, MathEmbed
from utils import load_commands


class HelpCommand(Command):
    """
    Inherits Command to implement the !signup help command.

    Provides the implementation to print out messages with help for using
    math-bot.
    """

    def __init__(self, client):
        """
        Get the available commands.
        """

        super().__init__(client)
        self.commands = None

    async def handle(self, message):
        """
        Prints out the help message for the bot to the channel the message
        originated from.

        message -- the discord.Message. Should be !help
        """

        # Load in commands if have not already
        if self.commands is None:
            self.commands = load_commands(self.client)

        split = message.content.split()
        if len(split) > 1:
            # User specified a chapter of help
            op = split[1]
            if op in ("-a", "--aliases"):
                await self._send_alias_help(message)
            else:
                # Try to send command help message
                for command in self.commands.values():
                    if op in command.aliases():
                        await command.send_help(message.channel)
                        break
                else:
                    # Invalid command
                    await self._send_default_help(message)
        else:
            await self._send_default_help(message)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("help", )

    def description(self):
        """
        Returns the description of this command.
        """

        return "**" + Command.PREFIX + "help:** display this message"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Displays the help messages and command help.\n\n" \
            "Usage:\n" \
            "- **" + Command.PREFIX + "help**: display the help message\n" \
            "- **" + Command.PREFIX + "help -a**: display available aliases\n" \
            "- **" + Command.PREFIX + "help command**: display the help for " \
            "a command"
        await self.send_help_message(channel, msg)

    async def _send_default_help(self, message):
        """
        Sends the default help message to the channel message was sent in

        message -- the discord.Message that triggered the help message
        """

        msg = "__Available commands:__\n"
        for command in sorted(self.commands.values(), key=lambda c: str(c)):
            msg += "- %s\n" % command.description()

        msg += "\nFor command aliases use: **" + Command.PREFIX + "help " \
               "--aliases**\n" \
               "For command help use: **" + Command.PREFIX + "help command**\n"\
               "More information: https://tirtow.gitlab.io/math-bot"
        embed = MathEmbed(self.client.user,
                          color=Color.INFO,
                          title="math-bot Help",
                          description=msg)
        await message.channel.send(embed=embed)

    async def _send_alias_help(self, message):
        """
        Sends the alias help message to the channel message came from

        message -- the discord.Message that triggered the help message
        """

        msg = "__Available command aliases:__\n"
        msg += self._build_alias_help(self.commands.values())
        embed = MathEmbed(self.client.user,
                          color=Color.INFO,
                          title="math-bot Alias Help",
                          description=msg)
        await message.channel.send(embed=embed)

    def _build_alias_help(self, alias_list):
        """
        Builds the alias help message for the given list of aliases

        alias_list -- the list of aliases
        """

        result = ""
        for entry in sorted(alias_list, key=lambda e: str(e)):
            if len(entry.aliases()) > 0:
                it = iter(entry.aliases())
                result += "**" + next(it) + ":** "
                count = len(entry.aliases()) - 1
                for alias in it:
                    result += alias
                    if count > 0:
                        result += ", "
                        count -= 1
                result += "\n"
        return result
