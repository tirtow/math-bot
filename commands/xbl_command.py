from commands.command import Command
from commands.info_command_ext import InfoCommand


class XblCommand(InfoCommand):
    """
    Inherits Command to implement the !xbl <name> command.

    Handles searching for a player on the xbox live platform
    """

    async def handle(self, message):
        """
        Searches for the given player on the Xbox Live platform

        message -- the discord.Message. Should be !help
        """

        await self.search_for_player("xbl", message)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("xbl", "xbox")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**" + Command.PREFIX + "xbl <name> [compare]:** search for " \
               "the player with the given name on the Xbox Live platform " \
               "or compare two players"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Searches for the user with the given name on the Xbox Live " \
              "platform. If a second name is provided compares the two "\
              "players.\n\n" \
            "Usage:\n" \
            "- **" + Command.PREFIX + "xbl <name>**: search for name\n" \
            "- **" + Command.PREFIX + "xbl <name> <compare>**: compare the " \
            "two names given\n"
        await self.send_help_message(channel, msg)
