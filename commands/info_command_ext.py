from commands.command import Command
from math_embed import Color, MathEmbed
from math_exceptions import MissingArgumentError, NoPlayerFoundError
from player import Player


class InfoCommand(Command):
    """
    Extends Command to provide a method to search for a single player and show
    their information.

    Does not fully implement Command, inheriting classes should fully implement
    Command.
    """

    async def search_for_player(self, platform, message):
        """
        Handles searching for a player and displaying results.

        platform    -- the platform to search on
        message     -- the discord.Message that was received
        """

        split = message.content.lower().split()
        if len(split) < 2:
            # User did not supply a name to search
            raise MissingArgumentError("Missing require argument name.\n" +
                                       "Use `!help " + platform + "` for " +
                                       "more information.")

        names = split[1:]
        if len(names) == 1:
            await self._display_player(message.channel, platform, names[0])
        else:
            await self._compare_players(message.channel,
                                        platform,
                                        names[0],
                                        names[1])

    async def _display_player(self, channel, platform, name):
        """
        Displays information for a single player

        channel     -- the discord.Channel to display to
        platform    -- the platform to search
        name        -- the name to search for
        """

        # Get the Players
        players = await Player.get(self.client, platform, name)

        # Find the Player with name
        player = await self._find_player(channel, platform, name, players)
        if player is not None:
            # Display the information
            await player.get_full_info()
            await player.send_info(channel)

    async def _compare_players(self, channel, platform, base_name,
                               compare_name):
        """
        Compares one Player against another Player

        channel         -- the discord.Channel to display to
        platform        -- the platform to search
        base_name       -- the name to use as the base Player
        compare_name    -- the name to use for the Player to compare against
                           the base player
        """

        # Get the base Player
        players = await Player.get(self.client, platform, base_name)
        base_player = await self._find_player(channel,
                                              platform,
                                              base_name,
                                              players)

        if base_player is not None:
            # Get the compare Player
            players = await Player.get(self.client, platform, compare_name)
            compare_player = await self._find_player(channel,
                                                     platform,
                                                     compare_name,
                                                     players)

            if compare_player is not None:
                # Get the full info for each player
                await base_player.get_full_info()
                await compare_player.get_full_info()

                # Compare the players
                await base_player.compare_with_player(channel, compare_player)

    async def _find_player(self, channel, platform, name, players):
        """
        Searches for the Player whose name matches the given name.

        channel     -- the discord.Channel to display to
        platform    -- the platform that was searched
        name        -- the name to search for
        players     -- the list of Players to search through

        Returns the Player if found, None otherwise
        """

        # Ensure some result was returned
        if len(players) == 0:
            raise NoPlayerFoundError("No players found with the name **" +
                                     name + "** on the platform **" +
                                     platform + "**")

        if len(players) == 1:
            # Only have one result, return it
            return players[0]

        # Search returned multiple results, search for the Player with the
        # name that matches the given name exactly
        for player in players:
            if player.name.lower() == name:
                # Get the full information and display it
                return player
        else:
            # Did not find a player with a matching name
            await self._multiple_matches(channel,
                                         platform,
                                         name,
                                         players)
            return None

    async def _multiple_matches(self, channel, platform, name, players):
        """
        Alerts the user that multiple matches for the given name were found.

        channel     -- the discord.Channel to send to
        platform    -- the platform that was searched on
        name        -- the name that was used to search
        players     -- the Players that were matched
        """

        msg = "Found multiple matches for **" + name + "** on the platform " \
            "**" + platform + "**.\nPlease specify a full name."
        embed = MathEmbed(self.client.user,
                          color=Color.ERROR,
                          title="Multiple Matches",
                          description=msg)

        names = ""
        for player in players:
            names += "- `" + player.name + "`\n"
        embed.add_field(name="First 30 Matched Names", value=names)

        await channel.send(embed=embed)
