from math_embed import Color, MathEmbed


class Command:
    """
    The implementation for a command that is recognized by the bot.

    The class name should be NameCommand and the file name should be
    name_command.py

    Inheriting classes must implement the handle() method to provide the
    functionality for the command. They must also implement the aliases() method
    to get an iterable of the aliases for this Command.
    """

    PREFIX = "::"   # The prefix for commands

    def __init__(self, client):
        """
        Initializes this Command with the given client

        client  -- the client running the bot
        """

        self.client = client

    async def handle(self, message):
        """
        Handle the given message.

        Must be implemented by any classes inheriting from Command.

        message     -- the discord.Message to handle.
        """

        raise NotImplementedError("handle must be implemented by " + str(self))

    def aliases(self):
        """
        Gets an iterable of the aliases for this command.

        Must be implemented by any classes inheriting from Command.
        """

        raise NotImplementedError("aliases must be implemented by " + str(self))

    def description(self):
        """
        Gets a description of this command.

        Must be implemented by any classes inheriting from Command.
        """

        raise NotImplementedError("description must be implemented by " +
                                  str(self))

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        Must be implemented by any classes inheriting from Command.
        """

        raise NotImplementedError("send_help must be implemented by " +
                                  str(self))

    def is_alias(self, alias):
        """
        Get whether or not the given string is an alias of this command.

        alias   -- the string to check

        Returns True if is an alias, False otherwise
        """

        return alias in self.aliases()

    async def send_help_message(self, channel, msg):
        """
        Handles sending the given help message to the channel

        channel     -- the discord.Channel to send to
        msg         -- the message to send
        """

        msg += "\n\nAliases:"
        for alias in self.aliases():
            msg += "\n- %s" % alias

        embed = MathEmbed(self.client.user,
                          color=Color.INFO,
                          title="!%s usage" % self.aliases()[0],
                          description=msg)
        await channel.send(embed=embed)

    def __str__(self):
        """
        Return the type of self.
        """

        return str(type(self))
