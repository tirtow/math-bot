from commands.command import Command
from commands.info_command_ext import InfoCommand


class UplayCommand(InfoCommand):
    """
    Inherits Command to implement the !uplay <name> command.

    Handles searching for a player on the uplay platform
    """

    async def handle(self, message):
        """
        Searches for the given player on the uplay platform.

        message -- the discord.Message. Should be !help
        """

        await self.search_for_player("uplay", message)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("uplay", "pc")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**" + Command.PREFIX + "uplay <name> [compare]:** search for " \
               "the player with the given name on the uplay platform or " \
               "compare two players"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Searches for the user with the given name on the uplay " \
              "platform. If a second name is provided compares the two "\
              "players.\n\n" \
            "Usage:\n" \
            "- **" + Command.PREFIX + "uplay <name>**: search for name\n" \
            "- **" + Command.PREFIX + "uplay <name> <compare>**: compare the " \
            "two names given\n"
        await self.send_help_message(channel, msg)
