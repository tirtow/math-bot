from commands.command import Command
from math_embed import Color, MathEmbed


class AboutCommand(Command):
    """
    Inherits Command to implement the !about command
    """

    async def handle(self, message):
        """
        Displays the about message for the bot.

        message -- the discord.Message. Should be !about
        """

        msg = \
            "<@" + str(self.client.user.id) + "> is a Discord bot developed " \
            "by tirtow to display player information for The Division 2.\n\n" \
            "More information: https://tirtow.gitlab.io/math-bot/\n" \
            "Source code: https://gitlab.com/tirtow/math-bot\n" \
            "More projects by tirtow: " \
            "https://tirtow.gitlab.io/"
        embed = MathEmbed(self.client.user,
                          title="About math-bot",
                          description=msg,
                          color=Color.INFO)
        await message.channel.send(embed=embed)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("about",)

    def description(self):
        """
        Returns the description of this command.
        """

        return "**" + Command.PREFIX + "about:** display information about " \
               "the bot"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Displays information about this bot.\n\n" \
            "Usage:\n" \
            "- **" + Command.PREFIX + "about**\n"
        await self.send_help_message(channel, msg)
