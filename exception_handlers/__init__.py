"""
The handlers for exceptions encountered when math-bot executes a `Command`.

Exception handlers are loaded in at runtime using reflection. Only handlers in
files ending in "_exception_handler.py" are loaded in.

ExceptionHandlers must follow the naming structure:
- filename:   some_name_exception_handler.py
- class name: SomeNameExceptionHandler.py

Exception handlers **must** respond to the methods that `ExceptionHandler`
responds to in order to integrate properly with math-bot. See
`help(ExceptionHandler)` for more information.
"""
