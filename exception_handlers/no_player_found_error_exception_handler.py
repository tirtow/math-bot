from math_exceptions import NoPlayerFoundError
from exception_handlers.exception_handler import ExceptionHandler


class NoPlayerFoundErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle NoPlayerFoundError.
    """

    async def handle(self, message, error, title="math-bot Error"):
        """
        Sends a message telling the user that no player was found for the search
        query.

        message -- the discord.Message that was received
        error   -- the error message received
        title   -- the title for the Embed. Default is "Error"
        """

        await self.send_error_message(message.channel, title, error)

    def exception_type(self):
        """
        Returns the NoPlayerFoundError type.
        """

        return NoPlayerFoundError
