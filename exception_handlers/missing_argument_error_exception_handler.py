from math_exceptions import MissingArgumentError
from exception_handlers.exception_handler import ExceptionHandler


class MissingArgumentErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle MissingArgumentError.
    """

    async def handle(self, message, error, title="math-bot Error"):
        """
        Sends a message telling the user they are missing a required argument.

        message -- the discord.Message that was received
        error   -- the error message received
        title   -- the title for the Embed. Default is "Error"
        """

        await self.send_error_message(message.channel, title, error)

    def exception_type(self):
        """
        Returns the MissingArgumentError type.
        """

        return MissingArgumentError
