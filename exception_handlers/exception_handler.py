from math_embed import Color, MathEmbed


class ExceptionHandler:
    """
    The implementation for an Exception handler for the bot.

    The class name should be NameExceptionHandler and the file name should be
    name_exception_handler.py

    Inheriting classes must implement the handle() method to provide the
    functionality for the command. They must also implement the exception_type()
    method to provide the type of exception that this handler handles.
    """

    def __init__(self, client):
        """
        Initializes this ExceptionHandler with the given client

        client  -- the client running the bot
        """

        self.client = client

    async def handle(self, message, error, title="Error"):
        """
        Handle the Exception this ExceptionHandler handles.

        Must be implemented by any classes inheriting from ExceptionHandler.

        message     -- the discord.Message that triggered the Exception
        error       -- the error message received
        title       -- the title. (default "Error")
        """

        raise NotImplementedError("handle must be implemented by " + str(self))

    def exception_type(self):
        """
        Returns the type of the Exception handled by this ExceptionHandler.

        Must be implemented by any classes inheriting from ExceptionHandler.
        """

        raise NotImplementedError("exception_type must be implemented by " +
                                  str(self))

    def can_handle(self, exception_type):
        """
        Gets whether or not the given Exception type can be handled.

        exception_type  -- the type of the Exception

        Returns True if can handle the given Exception type, False otherwise
        """

        return exception_type == self.exception_type()

    async def send_error_message(self, channel, title, msg):
        """
        Builds the embed and sends the error message

        channel     -- the discord.Channel to send the message to
        title       -- the title of the embed
        msg         -- the message for the embed
        """

        embed = MathEmbed(self.client.user,
                          color=Color.ERROR,
                          title=title,
                          description=msg)
        await channel.send(embed=embed)

    def __str__(self):
        """
        Returns the type of self
        """

        return str(type(self))
