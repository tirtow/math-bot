class MissingArgumentError(Exception):
    """
    Thrown to signal that the command is missing an argument
    """


class NoPlayerFoundError(Exception):
    """
    Thrown to signal that no player was found with the given information
    """
