import aiohttp
import json

from math_embed import Color, MathEmbed
from math_exceptions import NoPlayerFoundError
from utils import secs_to_human, compare_values, compare_times


class Player:
    """
    Represents the information for a single player from the API.

    Provides static methods to get players based on platform and name and allows
    for pulling the full information for a player.
    """

    def __init__(self, client, pid, name):
        """
        Initializes a new Player

        client  -- the client running the bot
        pid     -- the PID of the Player
        name    -- the name of the Player
        """

        self.client = client
        self.pid = pid
        self.name = name

    async def get_full_info(self):
        """
        Loads the full information for this Player
        """

        url = "https://thedivisiontab.com/api/player.php?pid=%s" % self.pid
        info = await Player._fetch(url)

        if not info["playerfound"]:
            # Could not find the player
            raise NoPlayerFoundError("No player found with the pid **" +
                                     self.pid + "**")

        # Set the attributes
        self._set_attributes(info)

    async def send_info(self, channel):
        """
        Sends the information for this Player to the given channel

        channel     -- the discord.Channel to send to
        """

        description = "Level %s - Dark Zone Level %s" % (str(self.level_pve),
                                                         str(self.level_dz))
        embed = MathEmbed(self.client.user,
                          color=Color.INFO,
                          title="Player Information for %s" % self.name,
                          description=description)
        embed.set_thumbnail(url=self.avatar_256)

        # Add the basic information
        embed.add_field(name="Information",
                        value=self._basic_info(),
                        inline=False)

        # Add the playtime information
        embed.add_field(name="Playtime", value=self._playtime(), inline=False)

        # Add the kills summary
        embed.add_field(name="Kill Summary", value=self._kills())

        # Add the PvE kills information
        embed.add_field(name="PvE Kills", value=self._kills_pve())

        # Add the Dark Zone PvE kills information
        embed.add_field(name="Dark Zone PvE Kills", value=self._kills_dz())

        # Add the PvP kills information
        embed.add_field(name="PvP Kills", value=self._kills_pvp())

        # Add the Weapon kills information
        embed.add_field(name="Weapon Kills", value=self._kills_weapons())

        await channel.send(embed=embed)

    async def compare_with_player(self, channel, other):
        """
        Compares this Player against other

        channel     -- the discord.Channel to display to
        other       -- the Player to compare against this Player
        """

        description = "Comparing %s against %s" % (self.name, other.name)
        embed = MathEmbed(self.client.user,
                          color=Color.INFO,
                          title="Comparison between %s and %s" % (self.name,
                                                                  other.name),
                          description=description)

        # Add the basic information
        embed.add_field(name="Information",
                        value=self._basic_info(other),
                        inline=False)

        # Add the playtime information
        embed.add_field(name="Playtime",
                        value=self._playtime(other),
                        inline=False)

        # Add the kills summary
        embed.add_field(name="Kill Summary", value=self._kills(other))

        # Add the PvE kills information
        embed.add_field(name="PvE Kills", value=self._kills_pve(other))

        # Add the Dark Zone PvE kills information
        embed.add_field(name="Dark Zone PvE Kills",
                        value=self._kills_dz(other))

        # Add the PvP kills information
        embed.add_field(name="PvP Kills", value=self._kills_pvp(other))

        # Add the Weapon kills information
        embed.add_field(name="Weapon Kills", value=self._kills_weapons(other))

        await channel.send(embed=embed)

    def _basic_info(self, other=None):
        """
        Gets the basic info string for this Player.

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        level = str(self.level_pve)
        dz_level = str(self.level_dz)
        headshots = "{:,}".format(self.headshots)
        looted = "{:,}".format(self.looted)
        specialization = self.specialization
        gearscore = "{:,}".format(self.gearscore)
        ecredits = "{:,}".format(self.ecredits)

        if other is not None:
            level += compare_values(self.level_pve, other.level_pve)
            dz_level += compare_values(self.level_dz, other.level_dz)
            headshots += compare_values(self.headshots, other.headshots)
            looted += compare_values(self.looted, other.looted)
            gearscore += compare_values(self.gearscore, other.gearscore)
            ecredits += compare_values(self.ecredits, other.ecredits)

            if other.specialization != "":
                specialization += " (%s)" % other.specialization

        return \
            "- **Level**: %s\n" % level + \
            "- **Dark Zone Level**: %s\n" % dz_level + \
            "- **Headshots**: %s\n" % headshots + \
            "- **Items Looted**: %s\n" % looted + \
            "- **Specialization**: %s\n" % specialization + \
            "- **Gear Score**: %s\n" % gearscore + \
            "- **E-Credits**: %s\n" % ecredits

    def _playtime(self, other=None):
        """
        Gets the playtime string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        total = str(secs_to_human(self.timeplayed_total))
        pve = str(secs_to_human(self.timeplayed_pve))
        pvp = str(secs_to_human(self.timeplayed_pvp))
        dark_zone = str(secs_to_human(self.timeplayed_dz))
        rouge = str(secs_to_human(self.timeplayed_rogue))

        if other is not None:
            total += compare_times(self.timeplayed_total,
                                   other.timeplayed_total)
            pve += compare_times(self.timeplayed_pve,
                                 other.timeplayed_pve)
            pvp += compare_times(self.timeplayed_pvp,
                                 other.timeplayed_pvp)
            dark_zone += compare_times(self.timeplayed_dz,
                                       other.timeplayed_dz)
            rouge += compare_times(self.timeplayed_rogue,
                                   other.timeplayed_rogue)

        return \
            "- **Total**: %s\n" % total + \
            "- **PvE**: %s\n" % pve + \
            "- **PvP**: %s\n" % pvp + \
            "- **Dark Zone**: %s\n" % dark_zone + \
            "- **Rouge**: %s\n" % rouge

    def _kills(self, other=None):
        """
        Gets the kills string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        total = str(self.kills_total)
        npc = str(self.kills_npc)
        pvp = str(self.kills_pvp)
        headshot = str(self.kills_headshot)
        bleeding = str(self.kills_bleeding)
        shocked = str(self.kills_shocked)
        burning = str(self.kills_burning)
        ensnare = str(self.kills_ensnare)
        skill = str(self.kills_skill)
        turret = str(self.kills_turret)

        if other is not None:
            total += compare_values(self.kills_total, other.kills_total)
            npc += compare_values(self.kills_npc, other.kills_npc)
            pvp += compare_values(self.kills_pvp, other.kills_pvp)
            headshot += compare_values(self.kills_headshot,
                                       other.kills_headshot)
            bleeding += compare_values(self.kills_bleeding,
                                       other.kills_bleeding)
            shocked += compare_values(self.kills_shocked, other.kills_shocked)
            burning += compare_values(self.kills_burning, other.kills_burning)
            ensnare += compare_values(self.kills_ensnare, other.kills_ensnare)
            skill += compare_values(self.kills_skill, other.kills_skill)
            turret += compare_values(self.kills_turret, other.kills_turret)

        return \
            "- **Total**: %s\n" % total + \
            "- **NPC**: %s\n" % npc + \
            "- **PvP**: %s\n" % pvp + \
            "- **Headshot**: %s\n" % headshot + \
            "- **Bleeding**: %s\n" % bleeding + \
            "- **Shocked**: %s\n" % shocked + \
            "- **Burning**: %s\n" % burning + \
            "- **Ensnared**: %s\n" % ensnare + \
            "- **Skill**: %s\n" % skill + \
            "- **Turret**: %s\n" % turret

    def _kills_pve(self, other=None):
        """
        Gets the PvE kills string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        hyenas = str(self.kills_pve_hyenas)
        outcasts = str(self.kills_pve_outcasts)
        truesons = str(self.kills_pve_truesons)
        blacktusks = str(self.kills_pve_blacktusk)

        if other is not None:
            hyenas += compare_values(self.kills_pve_hyenas,
                                     other.kills_pve_hyenas)
            outcasts += compare_values(self.kills_pve_outcasts,
                                       other.kills_pve_outcasts)
            truesons += compare_values(self.kills_pve_truesons,
                                       other.kills_pve_truesons)
            blacktusks += compare_values(self.kills_pve_blacktusk,
                                         other.kills_pve_blacktusk)

        return \
            "- **Hyenas**: %s\n" % hyenas + \
            "- **Outcasts**: %s\n" % outcasts + \
            "- **True Sons**: %s\n" % truesons + \
            "- **Black Tusks**: %s\n" % blacktusks

    def _kills_dz(self, other=None):
        """
        Gets the Dark Zone PvE kills string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        hyenas = str(self.kills_pve_dz_hyenas)
        outcasts = str(self.kills_pve_dz_outcasts)
        truesons = str(self.kills_pve_dz_truesons)
        blacktusks = str(self.kills_pve_dz_blacktusk)

        if other is not None:
            hyenas += compare_values(self.kills_pve_dz_hyenas,
                                     other.kills_pve_dz_hyenas)
            outcasts += compare_values(self.kills_pve_dz_outcasts,
                                       other.kills_pve_dz_outcasts)
            truesons += compare_values(self.kills_pve_dz_truesons,
                                       other.kills_pve_dz_truesons)
            blacktusks += compare_values(self.kills_pve_dz_blacktusk,
                                         other.kills_pve_dz_blacktusk)

        return \
            "- **Hyenas**: %s\n" % hyenas + \
            "- **Outcasts**: %s\n" % outcasts + \
            "- **True Sons**: %s\n" % truesons + \
            "- **Black Tusks**: %s\n" % blacktusks

    def _kills_pvp(self, other=None):
        """
        Gets the PvP kills string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        total = str(self.kills_pvp_dz_total)
        rogue = str(self.kills_pvp_dz_rogue)
        namedbosses = str(self.kills_pvp_namedbosses)
        elitebosses = str(self.kills_pvp_elitebosses)

        if other is not None:
            total += compare_values(self.kills_pvp_dz_total,
                                    other.kills_pvp_dz_total)
            rogue += compare_values(self.kills_pvp_dz_rogue,
                                    other.kills_pvp_dz_rogue)
            namedbosses += compare_values(self.kills_pvp_namedbosses,
                                          other.kills_pvp_namedbosses)
            elitebosses += compare_values(self.kills_pvp_elitebosses,
                                          other.kills_pvp_elitebosses)

        return \
            "- **Dark Zone Total**: %s\n" % total + \
            "- **Dark Zone Rogue**: %s\n" % rogue + \
            "- **Named Bosses**: %s\n" % namedbosses + \
            "- **Elite Bosses**: %s\n" % elitebosses

    def _kills_weapons(self, other=None):
        """
        Gets the Weapon kills string for this Player

        If another Player is provided, compares this Player against the other
        Player.

        other   -- the Player to compare against (default None).
        """

        pistol = str(self.kills_wp_pistol)
        smg = str(self.kills_wp_smg)
        rifles = str(self.kills_wp_rifles)
        shotgun = str(self.kills_wp_shotgun)
        grenade = str(self.kills_wp_grenade)
        specialization = str(self.kills_specialization)

        if other is not None:
            pistol += compare_values(self.kills_wp_pistol,
                                     other.kills_wp_pistol)
            smg += compare_values(self.kills_wp_smg, other.kills_wp_smg)
            rifles += compare_values(self.kills_wp_rifles,
                                     other.kills_wp_rifles)
            shotgun += compare_values(self.kills_wp_shotgun,
                                      other.kills_wp_shotgun)
            grenade += compare_values(self.kills_wp_grenade,
                                      other.kills_wp_grenade)
            specialization += compare_values(self.kills_specialization,
                                             other.kills_specialization)

        return \
            "- **Pistol**: %s\n" % pistol + \
            "- **SMG**: %s\n" % smg + \
            "- **Rifle**: %s\n" % rifles + \
            "- **Shotgun**: %s\n" % shotgun + \
            "- **Grenade**: %s\n" % grenade + \
            "- **Specialization**: %s\n" % specialization

    def _set_attributes(self, info):
        """
        Sets the selected keys from info as attributes of this Player

        info    -- the dict of player info
        """

        keys = ("ecredits", "level_pve", "level_dz", "xp_clan", "xp_dz",
                "xp_ow", "xp_pvp", "timeplayed_total", "timeplayed_dz",
                "timeplayed_pve", "timeplayed_pvp", "timeplayed_rogue",
                "kills_npc", "kills_pvp", "kills_total", "kills_bleeding",
                "kills_shocked", "kills_burning", "kills_ensnare",
                "kills_headshot", "kills_skill", "kills_turret",
                "kills_pvp_namedbosses", "kills_pvp_elitebosses",
                "kills_pvp_dz_total", "kills_pvp_dz_rogue", "kills_pve_hyenas",
                "kills_pve_outcasts", "kills_pve_blacktusk",
                "kills_pve_truesons", "kills_pve_dz_hyenas",
                "kills_pve_dz_outcasts", "kills_pve_dz_blacktusk",
                "kills_pve_dz_truesons", "kills_wp_pistol", "kills_wp_grenade",
                "kills_wp_smg", "kills_wp_shotgun", "kills_wp_rifles",
                "kills_specialization", "specialization", "headshots", "looted",
                "gearscore", "avatar_256")

        for key in keys:
            self.__dict__[key] = info[key]

    @staticmethod
    async def get(client, platform, name):
        """
        Gets the Players for the given platform and name

        client      -- the client running the bot
        platform    -- the platform to search on
        name        -- the name to lookup

        Returns a list of Players
        """

        info = await Player._get_basic_info(platform, name)
        if info["totalresults"] == 0:
            return []

        return [Player(client, i["pid"], i["name"]) for i in info["results"]]

    @staticmethod
    async def _get_basic_info(platform, name):
        """
        Gets the basic info for this Player

        Returns the fetched results
        """

        # Get the info
        url = "https://thedivisiontab.com/api/search.php?platform=%s&name=%s" %\
            (platform, name)
        return await Player._fetch(url)

    @staticmethod
    async def _fetch(url):
        """
        Makes the request to the given url

        url     -- the url to request from

        Returns the response
        """

        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                return json.loads(await response.text())
